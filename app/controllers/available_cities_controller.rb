class AvailableCitiesController < ApplicationController
  def index
    @available_cities = Restaurant.all.map {|item| item.city}.uniq
  end
end
